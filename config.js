var config = module.exports = {};


// ******************** MQTT **************************************************
config.mqttId       = '';
config.mqttBroker   = '';
config.mqttPort     = '';
config.mqttUsername = '';
config.mqttPassword = '';


// ******************** REST **************************************************
config.restPort = 8080;
