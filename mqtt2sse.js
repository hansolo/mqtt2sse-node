var config  = require('./config.js');
var mqtt    = require('mqtt');
var restify = require('restify');
var cors    = require('cors');

var mqttConnected    = false;
var subscribedTopics = [];
var subscribers      = [];
var patterns         = {};

if (typeof String.prototype.startsWith !== 'function') {  // Add startsWith method to String
  String.prototype.startsWith = function(str) {
    return this.slice(0, str.length) === str;
  };
}
if (typeof String.prototype.endsWith !== 'function') {    // Add endsWith method to String
  String.prototype.endsWith = function(str) {
    return this.slice(-str.length) === str;
  };
}


// ******************** MQTT **************************************************
var mqttClient = mqtt.connect('mqtt://' + config.mqttUsername + ':' + config.mqttPassword + '@' + config.mqttBroker + ':' + config.mqttPort);
mqttClient.on('connect', function () { mqttConnected = true; });
mqttClient.on('offline', function() { mqttConnected = false; });
mqttClient.on('error', function() { mqttConnected = false; });
mqttClient.on('message', forwardToSSE);


// ******************** REST **************************************************
var server = restify.createServer();
server.use(restify.bodyParser());
server.use(restify.queryParser());
server.use(cors());


// ******************** Publish and Subscribe via REST and SSE ****************
server.post('/publish', publish);
function publish(req, res, next) {
  if (mqttConnected) {
    var mqttMsg = {
      'topic'    : req.params.topic.toString(),
      'msg'      : req.params.msg.toString(),
      'qos'      : parseInt(req.params.qos),
      'retained' : req.params.retained.toString() ===' true'
    };
    mqttClient.publish(mqttMsg.topic, mqttMsg.msg, {'qos': mqttMsg.qos, 'retained': mqttMsg.retained});
    res.writeHead(200, {'Content-Type': 'application/json; charset=utf-8'});
    res.end(JSON.stringify(mqttMsg));
    return next();
  }
  return next();
}

server.get('/subscribe', subscribe);
function subscribe(req, res, next) {
  res.connection.setTimeout(1000 * 60 * 20);

  res.writeHead(200, {
    'Content-Type' : 'text/event-stream',
    'Cache-Control': 'no-cache',
    'Connection'   : 'keep-alive'
  });
  res.write('retry: 10000\n');

  subscribers.push({"con" : res, "topic" : req.query.topic });

  if (mqttConnected) {
    var top = req.query.topic.toString();
    if (subscribedTopics.indexOf(top) === -1) {
      subscribedTopics.push(top);
      var topicQos  = {};
      topicQos[top] = parseInt(req.query.qos);
      mqttClient.subscribe(topicQos);
    }
  }

  req.once('close', function() {
    res.end();
    for (var i = 0, len = subscribers.length ; i < len ; i++) {
      if (subscribers[i].con === res) {
        if (i > -1) {
          subscribers.splice(i, 1);
          unSubscribeIfPossible(top);
          break;
        }
      }
    }
  });
  return next();
}
function forwardToSSE(topic, message) {
  for (var i = 0, len = subscribers.length ; i < len ; i++) {
    var res             = subscribers[i].con;
    var subscribedTopic = subscribers[i].topic.toString();
    var matchTopic      = subscribedTopic === topic;

    // Add pattern to registered patterns if not present
    if (!(subscribedTopic in patterns)) {
      var subTopics = subscribedTopic.split("/");
      var pattern = "";
      for (var j = 0, len2 = subTopics.length ; j < len2 ; j++) {
        pattern += j === 0 ? '' : '\\/';
        pattern += '(' + (subTopics[j] === '+' ? '(((\\+)|(\\w)*))' : subTopics[j]) + ')';
      }
      patterns[subscribedTopic] = new RegExp(pattern);
    }

    // Check topic against registered pattern incl. + wildcard
    var matchSingleLevel = patterns[subscribedTopic].test(topic);

    // Check topic against # wildcard
    var matchMultiLevel  = topic.startsWith(subscribedTopic.endsWith("#") ? subscribedTopic.substring(0, subscribedTopic.indexOf("#")) : subscribedTopic);

    // Send update only if topic is the requested one
    if(matchTopic || matchSingleLevel || matchMultiLevel) {
      var json     = {};
      json.topic   = topic;
      json.message = message.toString();
      res.write('data:' + JSON.stringify(json) +'\n\n');
    }
  }
}

function unSubscribeIfPossible(topic) {
  for(var i = 0, len = subscribers.length ; i < len ; i++) {
    if (subscribers[i].topic === topic) return;
  }
  mqttClient.unsubscribe(topic);
}


// ******************** Start REST service ************************************
server.listen(config.restPort, function() {
  console.log('%s listening at %s', server.name, server.url);
});
